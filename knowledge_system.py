import operator


class Graph:
    all_nodes = []
    actual_node = 0
    
    def add_node(self, node):
        self.all_nodes.append(node)

    def get_next_node(self, actual_node, input):
        self.actual_node = actual_node
        for id, weight in self.actual_node.adj_list.items():
            print(id)
            if weight == input:
                self.actual_node = self.all_nodes[id - 1]
        return self.actual_node

class Node:
    adj_list = {}
    is_target = 0

    def __init__(self, id, description):   
        self.id = id
        self.description = description
        self.adj_list = {}
        self.probability = [-1]*2

    def add_edge(self, dest_id, weight):
        self.adj_list[dest_id] = weight
        self.probability[weight] = 1

    def add_textOutput(self, text_output):
        self.text_output = text_output

    def print_adj(self):
        print(self.adj_list)

    def set_as_target(self):
        self.is_target = 1

    def get_more_probability_node(self):
        return self.probability

# Setup of the graph
node1 = Node(1, "Potrebujete databázu?")
node2 = Node(2, "Potrebujete mazať alebo inak upravovať už uložené dáta?")
node3 = Node(3, "Obsluhujú túto databázu viacerí nezávislí účastníci?")
node4 = Node(4, "Majú títo účastníci problém dohodnúť sa, kto bude spravovať databázu?")

node5 = Node(5, "Chcete aplikovať tradičné bankové služby na technológiu blockchain?")
node6 = Node(6, "Chcete si ponechať akúkoľvek kontrolu nad blockchainom, alebo to úplne prenechať sieti a jej používateľom?")
node7 = Node(7, "Je vaším hlavným cieľom budovať komunitu ľudí?")
node8 = Node(8, "Budete ukladať masívne dáta na blockchain?")
node9 = Node(9, "Potrebujete zložité smart kontrakty?")
node10 = Node(10, "Potrebujete aby bol obsah transakcií zašifrovaný?")
node11 = Node(11, "Chceli by ste verifikovať identitu bežných ľudí ?")
node12 = Node(12, "Chceli by ste uchovávať rôzne záznamy, z mnohých zdrojov?")
node13 = Node(13, "Potrebujete mikro transakcie?")
node14 = Node(14, "Potrebujete aby vaše transakcie boli súkromné a nikto o nich nevedel?")
node15 = Node(15, "Plánujete spolupracovať s bankami?")

final1 = Node(16, "Nepotrebujete blockchain") # Non blockchain
final2 = Node(17, "Steem / Bitshares") # 101
final3 = Node(18, "Sia / Storj") # 102
final4 = Node(19, "Etherneum") # 103
final5 = Node(20, "Bitcoin") # 104
final6 = Node(21, "Hyperledger Fabric") # 105
final7 = Node(22, "Hyperledger Indy") # 106
final8 = Node(23, "Hyperledger Sawtooth") # 107
final9 = Node(24, "IOTA") # 108
final10 = Node(25, "Corda") # 109
final11 = Node(26, "Stellar") # 109
final12 = Node(27, "Ripple") # 109

final13 = Node(28, "About")
final13.add_textOutput("")
final1.add_textOutput("")

final1.set_as_target()
final2.set_as_target()
final3.set_as_target()
final4.set_as_target()
final5.set_as_target()
final6.set_as_target()
final7.set_as_target()
final8.set_as_target()
final9.set_as_target()
final10.set_as_target()
final11.set_as_target()
final12.set_as_target()
final13.set_as_target()

final2.add_textOutput("""Ako je možné vidieť na obtázku odporúčané riešenie leží v pravom hornom
kvadrante. Tento kvadrant sa vynikajúce hodí na budovanie veľkých
komunít, pretože tu patrí DPoS protokol, ktorý je nato perfektný. Jeho
voľba lídrov, ktorý môžu ovplyvňovať sieť je presne na tento problém
pripravená. Toto riešenie sa má menšiu mieru anonymity overovateľov
oproti klasickému PoS, preto s v grafe nachádza nižšie ako Peercoinalebo Casper. 
Je to spôsobené tým, že sa volia daný lídri, ktorý musia byť známy. V prípade, že by ste sa rozhodli zachovať rovnosť všetkých
účastníkov, je možné použiť akýkoľvek verejnú permissionless blockchain
architektúru.""")
final3.add_textOutput("""
Tieto riešenia nenájdete v obrázku, pretože nie sú samostatnými blockchain
riešením. Jedná sa práve o unikátnu symbiózu, kde blockchain
zaznamenáva transakcie o dátach, ako: kto, kde a na ako dlho ich chce
uskladniť a tieto dáta sú potom pomocou rôznych mechanizmov uložené
na dostupnom mieste čo najbližšie ku objednávateľovi. Blockchain teda
neuchováva konkrétne dáta ale iba záväzok.""")
final4.add_textOutput("""
Etherneum riešenie sa nachádza v ľavom hornom
kvadrante, kde sa jedná o tie najviac distribuované siete, ktoré sú
ale veľmi energeticky náročné na prevádzku kvôli PoW (Proof of Work). Ako si môžete
všimnúť v momente, keď etherneum prejde na novú verziu, ktorá bude
používať Casper, posunie sa viacej doprava ku väčšej dôvere v jednotlivého
overovateľa. Táto zmena spôsobí zvýšenie rizika centralizácie ale
sieť už nebude taká náročná na prevádzku. Je to cesta ktorou sa rozhodli
vývojári ísť aby výrazné zlepšili škálovanie a flexibilitu. Tietáto
technológia sa hodí na systémy, ktoré dokážu fungovať verejne a bez
potreby mať akúkoľvek kontrolu zvonku. Keďže sa jedná o verejný permissionless
blockchain všetky transakcie musia byť verejne prístupné a
to drasticky znižuje možné použitie v momentálnej dobe. Možno sa časom
dostaneme do bodu kedy sa zmenia hodnoty v spoločnosti a nebude
nám vadiť anonymná transparentnosť.""")
final5.add_textOutput("""
Ako prvý zo všetkých blockchain technológií vznikol Bitcoin. Jeho škálovácie a výkonnostné nedostatky sa 
žiaľ ešte nepodarilo vyriešiť. V obrázku sa nachádza v ľavom hornom kvadrante, patrí medzi tie najslobodnejšie
siete. Jeho blockchain nebol ešte nikdy prelomený a momentálne o najbezpečnejšie riešenie aké existuje. Následkom
tejto bezpečnosti je veľký spotreba elektrickej energie. Takisto nepodporuje zložité smart kontrakty a chýba mu modularita
ako pri iných novších riešeniach.""")
final6.add_textOutput("""
Hyperledger Fabric modulárna blockchainová platforma. Jej modularita
umožňuje používateľovi prispôsobiť si blockchain jednoducho podľa svojich
potrieb. Fabric sa nachádza v pravom dolnom kvadrante, kde sa
jedná o jedno z najflexibilnejších riešení v skupine súkromných permissioned
platforiem. Toto riešenie využíva skutočnosť, že je pripravené pre
enterprise riešenia naplno. Jeho implementácia hlasovacieho algoritmu
je veľmi rýchla a úsporná. Je to vďaka tomu, že všetci účastníci majú
overenú totožnosť a transakcie overujú iba niektorý z nich.""")
final7.add_textOutput(""" Jedná sa o modulárnu platformu pre overovanie totožnosti používateľov. 
Tento projekt má za cieľ drasticky znížiť náklady potrebné pre overenie totožnosti na internete.
Jeho funkcionalita spočíva v tom, že sa údaje zaznamenajú na blockchain a je ich možné potom pomocou 
jednoduchej funkcie overiť. Táto konkrétna implementácia je ešte stále iba vo fáze návrhu preto je v 
obrázku zaznačená prerušovanou líniou. V prípade úspešného nasadenia má tento systém potenciál kompletne
zmeniť spôsob ako firmy overujú svojich zákazníkov.""")
final8.add_textOutput("""
Táto technológia presahuje do každého kvadrantu, je to spôsobené jej
modularitou. Táto konkrétna technológia sa dá nakonfigurovať na každú
situáciu a využíva pritom ale stále PoET hlasovací mechanizmus. Táto
implementácia blockchain technológie ponúka nielen veľkú flexibilitu ale
vďaka PoET aj rýchlosť. Je to ideálny nástroj pre napríklad supply chain
manažment alebo správu interných skladov.""")
final9.add_textOutput("""
V tomto prípade sa nejedná o blockchain technológiu ale jej veľmi príbuznú obdobu nazvanú Tangle, 
preto ju nenájdete v obrázku. Táto technológia vyniká v množstve 
spracovaných transakcii a neexistujú tu poplatky za uskutočnenie transakcie. Je vhodná práve pre mikro 
transakcie, ktoré sú mnoho--krát nevýhodné pri tradičných blockchain technológiách pre ešte relatívne vysoké 
poplatky za transakcie. Využitie predovšetkým pre IoT (Internet of Things) platformy.""")
final10.add_textOutput("""
    Corda je zástupca plne súkromného systému, kde si absolútnu kontrolu
prenecháva prevádzkovateľ. Tento štýl systému je vhodný na vnútorné
enterprise systémy, kde ho používajú aj tak iba ľudia z firmy. Nie je
verejne prístupný a tak celá jeho pravosť a integrita leží na prevádzkovateľovi.
V prípade blockchainu Corda je možné aby boli transakcie
skryté a nikto o nich okrem odosielateľa a prijímateľa nevedel. Plné šifrovanie
je samozrejmosťou. Tento systém ma jediné vyžitie v enterprise
systémoch aj neprináša veľa výhod oproti iným enterprise riešeniam.""")
final11.add_textOutput("""
    Stellar vznikol ako hard fork od Ripple. Jeho cieľom je umožňovať globálne
platby bez potreby mať prostredníka a za čo najkratší čas. V grafe
sa nachádza medzi permissioned a permissionless, pretože má niečo z
oboch. Je síce decentralizovaný ale funguje na základe dôvery v iných
používateľov, ktorý musia vložiť dôveru vám. Jeho unikátna implementá-
cia hlasovacieho mechanizmu SCP, umožňuje zachovávať decentralizáciu
a vysokú rýchlosť spracovávania transakcií. Toto riešenie je veľmi vhodné
na základné transakcie, aj keď podporuje multisig transakcie nevie spracovávať
reálne smart kontrakty. Preto to je vhodné jedine ako platobná
brána a nie ako komplexná platforma na budovanie distribuovaných aplikácii.""")
final12.add_textOutput("""
Ripple si vybral inú cestu ako Stellar. Snaží sa zjednodušiť globálne
transakcie medzi bankami. V grafe sa nachádza v súkromných blockchainoch,
pretože iba overený a schválený používatelia môžu overovať
transakcie. Ripple bol zato veľmi kritizovaný a momentálne sa snaží
viac decentralizovať svoju sieť.""")

node1.add_edge(2, 1)
node1.add_edge(16, 0)
node2.add_edge(3, 0)
node2.add_edge(16, 1)
node3.add_edge(4, 1)
node3.add_edge(16, 0)
node4.add_edge(5, 1)
node4.add_edge(16, 0)
node5.add_edge(6, 0)
node5.add_edge(13, 1)
node6.add_edge(10, 1)
node6.add_edge(7, 0)
node7.add_edge(17, 1)
node7.add_edge(8, 0)
node8.add_edge(9, 0)
node8.add_edge(18, 1)
node9.add_edge(19, 1)
node9.add_edge(20, 0)
node10.add_edge(21, 1)
node10.add_edge(11, 0)
node11.add_edge(22, 1)
node11.add_edge(12, 0)
node12.add_edge(23, 1)
node12.add_edge(13, 0)
node13.add_edge(24, 1)
node13.add_edge(14, 0)
node14.add_edge(25, 1)
node14.add_edge(15, 0)
node15.add_edge(26, 0)
node15.add_edge(25, 1)

node1.add_textOutput("""
    Blockchain je zo svojej podstaty špeciálny typ
databázy, môžeme ho nazvať aj účtovná kniha.
Preto je táto elementárna otázka nesmierne dôležitá, je potrebné zvážiť či je databáza naozaj potrebná pre vaše potreby. V prípade, že nie
je databáza potrebná môžete zvážiť iné možnosti uchovávania dát ako napríklad Excel tabuľky alebo vyhľadávacie zoznamy.""")
node2.add_textOutput("""
    Táto otázka je zameraná na vylúčenie prípadov, kde sa veľmi aktívne zasahuje do
databázy. Blockchain neumožňuje meniť už uložené dáta. Nedovoľuje to
jeho vnútorná integrita, pretože by sa zmenil koreňový hash merklovho
stromu a museli by sa všetky bloky na novo overovať. Preto sa prípade, že
je nutné meniť už uložené dáta odporúča použiť inú tradičnú databázu.""")
node3.add_textOutput("""
    Táto otázka ma dve roviny, ta prvá je, či existuje viacero účastníkov, ktorý budú používať
databázu a ta druhá či sú nezávislí. V prípade, že nie je potrebné
aby túto databázu používali viacerí účastníci naraz nemá zmysel budovať
najkomplexnejšiu distribuovanú databázu akú poznáme (Blockchain). Stále totiž
platí, že je najlepšie mať dáta čo najbližšie. Nezávislosťou sa myslí, či to
nie sú len pobočky nejakej materskej inštancie, blockchain je zameraný
na distribuovanosť siete a decentralizáciu, ak existuje prísna hierarchia
účastníkov, ktorý budú databázu používať. Stráca toto riešenie efektívnosť
a vlastne aj zmysel.""")
node4.add_textOutput("""V bežnej prevádzke decentralizovanej databázy existujú situácie,
kde sa dvaja alebo viacerí účastníci musia dohodnúť na tom čo je
vlastne pravda. V týchto prípadoch sa väčšinou vyberie jeden účastník,
ktorý bude kontrolovať správnosť všetkého. Jedná sa v mnohých prípadoch
o nejaký hlavný uzol, ktorý ma právo všetko meniť. Mnohokrát
to je samotná firma, ktorá spravuje databázu. Ak nie je možné vybrať
takýto centrálny bod, či už preto lebo neexistuje alebo sa nedá zaručiť
jeho správne a poctivé správanie. Prichádzame do bodu kedy bude treba
naozaj distribuovanú peer-to-peer databázu akou je blockchain.""")
node5.add_textOutput("""
    Tradičné bankové služby zahŕňajú napríklad lízing, hypotéku,
pôžičky, predaj cenných papierov, dobropisov a iné. Problém s týmito
službami je, že veľmi nezapadajú do kontextu blockchain revolúcie. 
Banky dlhodobo používajú uzatvorené systémy a tento predpoklad vylučuje akúkoľvek verejnú kontrolu
respektíve v blockchain terminológií anonymitu overovateľov.""")
node6.add_textOutput("""
Toto je jedna z najťažších otázok, ktoré sa spájajú s výberom blockchain architektúry. 
Rozhodnutie medzi permissionless a permissioned typom architektúry rozhoduje nielen o tom, 
ktoré hlasovacie mechanizmy je možné použiť ale aj o tom ako veľmi chceme vložiť dôveru do 
rúk ľudí, ktorý budú tento systém používať. Každý človek si samozrejme chce prenechať nejakú kontrolu nad svojím výtvorom, 
no absolútne vzdanie sa akejkoľvek kontroly prináša mnoho výhod. Medzi tie hlavné patrí dôvera ľudí, že im s tým nebude nikto
 manipulovať. História ukázala, že štáty a iné inštitúcie sú ochotné špehovať a manipulovať svojich občanov. Vzdanie sa akejkoľvek
  kontroly eliminuje pozíciu, kde by sa niekto mohol pokúšať tlačiť alebo inak ovplyvňovať sieť, zákonmi alebo inými prostriedkami..""")
node7.add_textOutput("""
Účelom tejto otázky je lepšie určiť potrebný hlasovací mechanizmus. Aj keď má permissionless
blockchain mnoho variant hlasovacích mechanizmov, ktoré
môže použiť DPoS vyniká v svojej schopnosti budovať silnú a jednotnú
komunitu. Jeho systémom voľby jednotlivcov z pomedzi uchádzačov dokáže
naozaj vybrať najlepších lídrov pre komunitu a ľudia ju dokážu
aktívnejšie usmerňovať.""")
node8.add_textOutput("""
Blockchain uchováva všetky dáta permanentne. Je to veľmi drahý spôsob ako uchovávať veľké súbory. Preto si to vyžaduje špeciálne
upravenú architektúru.""")
node9.add_textOutput("""Smart kontrakt môže byť napríklad nezávislá zmluva na ktorej sa dohodnú dve strany, ktorá je 
automaticky vykonávaná na blockchaine. Rozhodujeme sa medzi dvomi veľmi podobnými technológiami, kde
jediné čo tieto technológie rozlišuje je turingová úplnosť a možnosť vytvárať zložité smart kontrakty.""")
node10.add_textOutput("""Jedným z hlavných argumentov proti permissionless blockchainu je nutnosť mať všetky
transakcie zverejnené. V prípade permissioned blockchainu to tak už nie
je. Preto sa naskytuje otázka, či treba teda obsah transakcií šifrovať.
Šifrovanie obsahu transakcii docieli, že ľudia síce budú vedieť zistiť či
s niekým obchodujete, ale už nie za koľko a pod akým kontraktom.
Táto informácia je viditeľná iba pre účastníkov transakcie.""")
node11.add_textOutput("""Overovanie identity (KYC) alebo Know your customer je jedna z najväčších nefinančných možností ako využiť blockchain technológiu.
    Tak ako každý špecifický prípad použitia si vyžaduje jedinečne upravenú architektúru aby mohla fungovať správne.""")
node12.add_textOutput("""Uchovávanie nemenných záznamov je jedna z veľkých výhod blockchainu.
V prípade, že by ste chceli tieto základné dátové štruktúry obsiahnuté v
merkle strome rozšíriť na niečo zložitejšie, ako napríklad celé objednávky
alebo sledovanie tovaru, sú tieto dátové štruktúry nepostačujúce. Preto je potrebná špecifická úprava blockchain architektúry.""")
node13.add_textOutput("""V prípade, že potrebujete mikro platby
alebo celý váš biznis plán pozostáva z mikro platieb nastáva pri použití
blockchainu problém, súvisí to s cenou transakcií.""")
node14.add_textOutput("""Existujú prípady kedy si neželáte aby niekto vedel že
s niekým vôbec obchodujete, poprípade sa nejedná o obchod ale o nejaký
smart kontrakt, ktorý je potrebné utajiť.""")
node15.add_textOutput("""
 To či počítate so spoluprácou s bankami je veľmi dôležité, pretože banky sa nechcú prispôsobiť novým pravidlám, ktoré blockchain
 určuje. """)

all_nodes = [value for name, value in locals().items() if name.startswith('node') or name.startswith('final')]
