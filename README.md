--- Practical part of bachelor thesis ---

This project was created as part of bachelor thesis on CTU in Prague. Its written in Python and uses Flask and FlaskWTForm for communication.
It helps user indentify blockchain technology needed for their needs.

 --- How to run --- 

python web.py

this opens socket on http://127.0.0.1:5000/?q=1

You can acces website by writing http://127.0.0.1:5000/?q=1 in webbrowser. Tested on Google Chrome.

 --- Dependencies --- 

Python
You can download python from website https://www.python.org/

Flask
Ninja2
FlaskWTF

 --- How to install dependencies ---

pip install flask

pip install flask_wtf

pip install ninja2

