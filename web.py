from flask import Flask, render_template, request, redirect, url_for
from flask_wtf import Form, CSRFProtect
from wtforms import RadioField
import knowledge_system
import os

app = Flask(__name__)
graph = knowledge_system.Graph()
graph.all_nodes = knowledge_system.all_nodes
csrf = CSRFProtect(app=app)
app.secret_key = os.urandom(24)

positive_answers = []


class AnswerForm(Form):
	answer = RadioField(choices=[('áno', 'Áno'), ('nie', 'Nie')])


@app.route('/', methods=['GET'])
def show_question():
	form = AnswerForm()
	question_number = int(request.args.get('q'))  # it is in query param (www.zns.com/?q=1)
	# we need to get node object that has question_number - 1
	actual_node = knowledge_system.all_nodes[question_number-1]
	description = actual_node.description
	text_output = actual_node.text_output
	if actual_node.is_target == 1:
			if actual_node.description == 'About':
				return render_template('about.html')
			elif actual_node.description == "Nepotrebujete blockchain":
				return render_template('final.html', description=description, positive_answers=positive_answers)
			return render_template('finalBlock.html', description=description, positive_answers=positive_answers,text_output=text_output)
	return render_template('question.html', description=description, form=form, text_output=text_output )


@app.route('/', methods=['POST'])
def get_another_question():
	form = AnswerForm()
	question_number = int(request.args.get('q'))
	actual_node = knowledge_system.all_nodes[question_number - 1]
	answer = request.form["answer"]
	if answer == "áno":
		answer_bool = 1
	elif answer == "nie":
		answer_bool = 0

	if answer_bool:
		positive_answers.append(actual_node)
	next_node = graph.get_next_node(actual_node=actual_node, input=answer_bool)
	query = "/?q=" + str(next_node.id)
	return redirect(query)


if __name__ == '__main__':
	app.run()
